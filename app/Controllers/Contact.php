<?php

namespace App\Controllers;

class Contact extends BaseController
{
    public function index()
    {
        $data = [
            'tittle' => 'Contact | Brileaf',
        ];
        return view('pages/contact', $data);
    }
}
