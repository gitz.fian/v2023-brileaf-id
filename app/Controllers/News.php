<?php

namespace App\Controllers;

class News extends BaseController
{
    public function index()
    {
        $data = [
            'tittle' => 'Events | Brileaf',
        ];
        return view('pages/news', $data);
    }

    public function nepal()
    {
        $data = [
            'tittle' => 'Events Details | Brileaf',
        ];
        return view('pages/news-nepal', $data);
    }
    public function aemfi()
    {
        $data = [
            'tittle' => 'Events Details | Brileaf',
        ];
        return view('pages/news-aemfi', $data);
    }
    public function uscmarshall()
    {
        $data = [
            'tittle' => 'Events Details | Brileaf',
        ];
        return view('pages/news-marshall', $data);
    }
    public function gsbthailand()
    {
        $data = [
            'tittle' => 'Events Details | Brileaf',
        ];
        return view('pages/news-gsbthailand', $data);
    }
}
