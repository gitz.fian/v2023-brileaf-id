<?php

namespace App\Controllers;

use App\Models\MainModel;
use App\Libraries\Zainlayout;
use Config\Services;

class Courses extends BaseController
{
    public function index()
    {
        $data = [
            'tittle' => 'Courses | Brileaf',
        ];
        return view('pages/courses2', $data);
    }

    // public function index()
    // {
    //     $main_model = new MainModel();
    //     $pager = Services::pager();
    //     $data = [];

    //     $zainlayout = new Zainlayout();
    //     $search = $this->request->getGet('search');
    //     $categoriesId = $this->request->getGet('categories');
    //     $limit = 12;
    //     $page = $this->request->getGet('page') ?? 1;
    //     $offset = ($page * $limit) - $limit;

    //     $count_all_courses = $main_model->get_count_courses($search, $categoriesId);
    //     $data['courses'] = $main_model->get_courses_all('', $search, $categoriesId, $limit, $offset);
    //     $data['paging'] = $pager->makeLinks($page, $limit, $count_all_courses[0]['total'], 'bootstrap_pagination');
    //     $data['categories'] = $main_model->get_categories_product();

    //     return $zainlayout->render('pages/courses', $data);
    // }

    public function pbk()
    {
        $data = [
            'tittle' => 'Competency Based Training | Brileaf',
        ];
        return view('pages/courses-pbk', $data);
    }
    public function certification()
    {
        $data = [
            'tittle' => ' Certification Courses | Brileaf',
        ];
        return view('pages/courses-certification', $data);
    }
    public function comodity()
    {
        $data = [
            'tittle' => ' Comodity Based Training | Brileaf',
        ];
        return view('pages/courses-comodity', $data);
    }
    public function tot()
    {
        $data = [
            'tittle' => ' Training Of Trainer | Brileaf',
        ];
        return view('pages/courses-tot', $data);
    }
    public function brief()
    {
        $data = [
            'tittle' => ' BRIEF | Brileaf',
        ];
        return view('pages/courses-brief', $data);
    }
    public function details()
    {
        $data = [
            'tittle' => 'Courses Details | Brileaf',
        ];
        return view('pages/courses-detail', $data);
    }
}
