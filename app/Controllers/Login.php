<?php

namespace App\Controllers;

use App\Models\MainModel;
use App\Libraries\Zainlayout;
use Config\Services;

class Login extends BaseController
{
    public function index()
    {
        $data = [
            'tittle' => 'Login | Brileaf',
        ];
        return view('pages/login', $data);
    }
}