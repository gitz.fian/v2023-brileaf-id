<?= $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>


<br>
<br>

<div class="blogarea__2 sp_top_100 sp_bottom_100">
    <div class="container">
        <div class="row">
            <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                <div class="blog__details__content__wraper">
                    <div class="blog__details__img" data-aos="fade-up">
                        <img src="https://brileaf.id/assets/landingpage/event-marshall.jpg" alt="blog">
                    </div>
                    <div class="blogarea__heading__2">
                        <h3>Cortesy Visit: Ibear 45 from USC Marshall University, California, AS</h3>
                    </div>
                    <div class="blog__details__content">
                        <p class="content__1" data-aos="fade-up">As the largest state-owned bank engaged in micro banking in Indonesia, PT. Bank BRI (persero) Tbk became the destination for the International Education and Research (IBEAR) Marshall School of Business, University of South California batch 45.</p>
                        <p class="content__2" data-aos="fade-up">On December 12, 2022, BRIef received visits from International Education and Research (IBEAR) Marshall School of Business University of South California batch 45. The visiting participants were 50 people from 12 countries, the average age of the participants is 36 years, and 13 years of work. The visit aims to obtain information on how BRI runs its business. The visit took place in the Majapahit Room and began with a welcome speech delivered by the President Director of the BRI Research Institute, Mr. Anton Hendranata. It ended with the handover of souvenirs by Mr. Anton Hendranata to Dr. Richard Drobnick-Director of the IBEAR MBA Program at USC Marshall School of Business.</p>
                        <p class="content__3" data-aos="fade-up">The presentation began with an explanation of the BRI Research Institute Company Profile by Mrs. Miryati Safitri - the Assistant Vice President, and on BRI Business by Mrs. Ety Yuniarti - Senior Vice President of BRI.</p>
                        <p class="content__4" data-aos="fade-up">Then in the evening, a deep conversation was held between Dr. Richard Drobnick and the BRI Research Institute represented by Mr. Zulfikar Nazara (Director of BRIRins Technical Assistance) and several BRIRins Leaders, namely Mrs. Miryati Safitri, Nilam Nirmala Anggraini and Mrs. Syifa Syafiatunnisa regarding business opportunities that exist at the USC Marshal School of Business and other universities connected to Dr. Richard Drobnick who can be worked on by BRIRins.</p>
                        <div class="blog__details__img" data-aos="fade-up">
                            <img src="https://brileaf.id/assets/img/galeri_10.JPG" alt="blog">
                        </div>

                        <div class="row blog__details__margin">
                            <div class="col-xl-4 col-lg-4 col-md-4" data-aos="fade-up">
                            </div>
                            <div class="col-xl-8 col-lg-8 col-md-8">
                            </div>
                        </div>
                    </div>
                    <div class="blog__details__tag" data-aos="fade-up">
                        <ul>
                            <li class="heading__tag">
                                Tag
                            </li>
                            <li>
                                <a href="#">Business</a>
                            </li>
                            <li>
                                <a href="#">Design</a>
                            </li>
                            <li>
                                <a href="#">apps</a>
                            </li>
                            <li>
                                <a href="#">data</a>
                            </li>
                        </ul>
                        <ul class="share__list" data-aos="fade-up">
                            <li class="heading__tag">
                                Share
                            </li>
                            <li>
                                <a href="#"><i class="icofont-twitter"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="icofont-facebook"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="icofont-instagram"></i></a>
                            </li>
                        </ul>
                    </div>

                    <div class="blog__details__comment" data-aos="fade-up">
                        <div class="blog__details__comment__heading">
                            <h5>(04) Comment</h5>
                        </div>
                        <div class="blog__details__comment__inner">
                            <div class="author__img">
                                <img src="<?= base_url('img/blog-details/blog-details__1.png'); ?>" alt="author">
                            </div>
                            <div class="author__content">
                                <div class="author__name">
                                    <h6><a href="#">Rohan De Spond</a></h6>
                                    <p>25 january 2023</p>

                                </div>
                                <div class="author__text">
                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have. There are many variations of passages of Lorem Ipsum available, but the majority have</p>
                                </div>
                            </div>
                            <div class="author__icon">
                                <button>
                                    <svg width="26" height="19" viewBox="0 0 26 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M5.91943 10.2031L12.1694 16.4531C13.3413 17.625 15.3726 16.8047 15.3726 15.125V12.3516C19.9819 12.5469 20.0991 13.5625 19.4351 15.8672C18.9272 17.5469 20.8413 18.9141 22.2866 17.9375C24.2788 16.5703 25.3726 14.8516 25.3726 12.3516C25.3726 6.76562 20.3726 5.67188 15.3726 5.47656V2.66406C15.3726 0.984375 13.3413 0.164062 12.1694 1.33594L5.91943 7.58594C5.17725 8.28906 5.17725 9.5 5.91943 10.2031ZM7.24756 8.875L13.4976 2.625V7.3125C18.1851 7.3125 23.4976 7.58594 23.4976 12.3516C23.4976 14.5391 22.3647 15.6328 21.2319 16.375C22.8335 11.0625 18.8491 10.4375 13.4976 10.4375V15.125L7.24756 8.875ZM0.919434 7.58594C0.177246 8.28906 0.177246 9.5 0.919434 10.2031L7.16943 16.4531C7.95068 17.2734 9.12256 17.1562 9.82568 16.4531L2.24756 8.875L9.82568 1.33594C9.12256 0.632812 7.95068 0.515625 7.16943 1.33594L0.919434 7.58594Z" fill="#121416" />
                                    </svg>
                                </button>
                            </div>
                        </div>

                        <div class="blog__details__comment__inner author__padding__left" data-aos="fade-up">
                            <div class="author__img">
                                <img src="<?= base_url('img/blog-details/blog-details__2.png'); ?>" alt="author">
                            </div>
                            <div class="author__content">
                                <div class="author__name">
                                    <h6><a href="#">Rohan De Spond</a></h6>
                                    <p>25 january 2023</p>
                                </div>
                                <div class="author__text">
                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have. There are many variations of passages of Lorem Ipsum available, but the majority have</p>
                                </div>
                            </div>
                            <div class="author__icon">
                                <button>
                                    <svg width="26" height="19" viewBox="0 0 26 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M5.91943 10.2031L12.1694 16.4531C13.3413 17.625 15.3726 16.8047 15.3726 15.125V12.3516C19.9819 12.5469 20.0991 13.5625 19.4351 15.8672C18.9272 17.5469 20.8413 18.9141 22.2866 17.9375C24.2788 16.5703 25.3726 14.8516 25.3726 12.3516C25.3726 6.76562 20.3726 5.67188 15.3726 5.47656V2.66406C15.3726 0.984375 13.3413 0.164062 12.1694 1.33594L5.91943 7.58594C5.17725 8.28906 5.17725 9.5 5.91943 10.2031ZM7.24756 8.875L13.4976 2.625V7.3125C18.1851 7.3125 23.4976 7.58594 23.4976 12.3516C23.4976 14.5391 22.3647 15.6328 21.2319 16.375C22.8335 11.0625 18.8491 10.4375 13.4976 10.4375V15.125L7.24756 8.875ZM0.919434 7.58594C0.177246 8.28906 0.177246 9.5 0.919434 10.2031L7.16943 16.4531C7.95068 17.2734 9.12256 17.1562 9.82568 16.4531L2.24756 8.875L9.82568 1.33594C9.12256 0.632812 7.95068 0.515625 7.16943 1.33594L0.919434 7.58594Z" fill="#121416" />
                                    </svg>
                                </button>
                            </div>
                        </div>


                        <div class="blog__details__comment__inner" data-aos="fade-up">
                            <div class="author__img">
                                <img src="<?= base_url('img/blog-details/blog-details__3.png'); ?>" alt="author">
                            </div>
                            <div class="author__content">
                                <div class="author__name">
                                    <h6><a href="#">Rohan De Spond</a></h6>
                                    <p>25 january 2023</p>
                                </div>
                                <div class="author__text">
                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have. There are many variations of passages of Lorem Ipsum available, but the majority have</p>
                                </div>
                            </div>
                            <div class="author__icon">
                                <button>
                                    <svg width="26" height="19" viewBox="0 0 26 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M5.91943 10.2031L12.1694 16.4531C13.3413 17.625 15.3726 16.8047 15.3726 15.125V12.3516C19.9819 12.5469 20.0991 13.5625 19.4351 15.8672C18.9272 17.5469 20.8413 18.9141 22.2866 17.9375C24.2788 16.5703 25.3726 14.8516 25.3726 12.3516C25.3726 6.76562 20.3726 5.67188 15.3726 5.47656V2.66406C15.3726 0.984375 13.3413 0.164062 12.1694 1.33594L5.91943 7.58594C5.17725 8.28906 5.17725 9.5 5.91943 10.2031ZM7.24756 8.875L13.4976 2.625V7.3125C18.1851 7.3125 23.4976 7.58594 23.4976 12.3516C23.4976 14.5391 22.3647 15.6328 21.2319 16.375C22.8335 11.0625 18.8491 10.4375 13.4976 10.4375V15.125L7.24756 8.875ZM0.919434 7.58594C0.177246 8.28906 0.177246 9.5 0.919434 10.2031L7.16943 16.4531C7.95068 17.2734 9.12256 17.1562 9.82568 16.4531L2.24756 8.875L9.82568 1.33594C9.12256 0.632812 7.95068 0.515625 7.16943 1.33594L0.919434 7.58594Z" fill="#121416" />
                                    </svg>
                                </button>
                            </div>
                        </div>

                        <div class="blog__details__comment__inner author__padding__left" data-aos="fade-up">
                            <div class="author__img">
                                <img src="<?= base_url('img/blog-details/blog-details__4.png'); ?>" alt="author">
                            </div>
                            <div class="author__content">
                                <div class="author__name">
                                    <h6><a href="#">Rohan De Spond</a></h6>
                                    <p>25 january 2023</p>

                                </div>
                                <div class="author__text">
                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have. There are many variations of passages of Lorem Ipsum available, but the majority have</p>
                                </div>
                            </div>
                            <div class="author__icon">
                                <button>
                                    <svg width="26" height="19" viewBox="0 0 26 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M5.91943 10.2031L12.1694 16.4531C13.3413 17.625 15.3726 16.8047 15.3726 15.125V12.3516C19.9819 12.5469 20.0991 13.5625 19.4351 15.8672C18.9272 17.5469 20.8413 18.9141 22.2866 17.9375C24.2788 16.5703 25.3726 14.8516 25.3726 12.3516C25.3726 6.76562 20.3726 5.67188 15.3726 5.47656V2.66406C15.3726 0.984375 13.3413 0.164062 12.1694 1.33594L5.91943 7.58594C5.17725 8.28906 5.17725 9.5 5.91943 10.2031ZM7.24756 8.875L13.4976 2.625V7.3125C18.1851 7.3125 23.4976 7.58594 23.4976 12.3516C23.4976 14.5391 22.3647 15.6328 21.2319 16.375C22.8335 11.0625 18.8491 10.4375 13.4976 10.4375V15.125L7.24756 8.875ZM0.919434 7.58594C0.177246 8.28906 0.177246 9.5 0.919434 10.2031L7.16943 16.4531C7.95068 17.2734 9.12256 17.1562 9.82568 16.4531L2.24756 8.875L9.82568 1.33594C9.12256 0.632812 7.95068 0.515625 7.16943 1.33594L0.919434 7.58594Z" fill="#121416" />
                                    </svg>
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="blog__details__form" data-aos="fade-up">
                        <div class="blog__details__input__heading">
                            <h5>Write your comment</h5>
                        </div>
                        <div class="row">
                            <div class="col-xl-6">
                                <div class="blog__details__input">
                                    <input type="text" placeholder="Enter your name*">
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <div class="blog__details__input">
                                    <input type="text" placeholder="Enter your mail**">
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <div class="blog__details__input">
                                    <input type="text" placeholder="Enter your  number*">
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <div class="blog__details__input">
                                    <input type="text" placeholder="Website*">
                                </div>
                            </div>
                            <div class="col-xl-12">
                                <div class="blog__details__input">
                                    <textarea cols="30" rows="10">Enter your Massage*</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="blog__check__box">
                            <input type="checkbox" checked="checked"> Save my name, email, and website in this browser for the next time I comment.
                        </div>
                    </div>
                    <div class="blog__details__button">
                        <a class="default__button" href="#">Post a Comment</a>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                <div class="blogsidebar__content__wraper__2" data-aos="fade-up">
                    <h4 class="sidebar__title">Search here</h4>
                    <form action="#">
                        <div class="blogsudebar__input__area">
                            <input type="text" name="search" placeholder="Search product">
                            <button class="blogsidebar__input__icon">
                                <i class="icofont-search-1"></i>
                            </button>
                        </div>
                    </form>

                </div>
                <div class="blogsidebar__content__wraper__2" data-aos="fade-up">
                    <h4 class="sidebar__title">categories</h4>
                    <ul class="categorie__list">
                        <li><a href="#">BRIEF - USC Marshall <span>01</span></a></li>
                        <li><a href="#">BRIEF - Nepal <span>01</span></a></li>
                        <li><a href="#">BRIEF - AEMFI <span>01</span></a></li>
                        <li><a href="#">BRIEF - GSB Thailand <span>01</span></a></li>
                    </ul>
                </div>

                <div class="blogsidebar__content__wraper__2" data-aos="fade-up">
                    <h4 class="sidebar__title">Recent Post</h4>
                    <ul class="recent__list">
                        <li>
                            <div class="recent__img">
                                <a href="/news/gsbthailand">
                                    <img src="https://brileaf.id/assets/galeri/gsb6.JPG" style="width:136; height:78;" alt="sidbar">
                                    <div class="recent__number">
                                        <span>01</span>
                                    </div>
                                </a>
                            </div>
                            <div class="recent__text">
                                <div class="recent__date">
                                    <a href="/news/gsbthailand">08 March 2023</a>
                                </div>
                                <h6><a href="/news/gsbthailand">Cortesy Visit : Government Saving Bank Thailand </a></h6>
                            </div>
                        </li>
                        <li>
                            <div class="recent__img">
                                <a href="/news/aemfi">
                                    <img src="https://brileaf.id/assets/landingpage/event-aemfi.png" style="width:136; height:78;" alt="sidbar">
                                    <div class="recent__number">
                                        <span>02</span>
                                    </div>
                                </a>
                            </div>
                            <div class="recent__text">
                                <div class="recent__date">
                                    <a href="/news/aemfi">25 November 2022</a>
                                </div>
                                <h6><a href="/news/aemfi">Brief Program (BRI International Delegation Forum): Delegation Of... </a></h6>
                            </div>
                        </li>
                        <li>
                            <div class="recent__img">
                                <a href="/new/nepal">
                                    <img src="https://brileaf.id/assets/landingpage/event-nepal.png" style="width:136; height:78;" alt="sidbar">
                                    <div class="recent__number">
                                        <span>03</span>
                                    </div>
                                </a>
                            </div>
                            <div class="recent__text">
                                <div class="recent__date">
                                    <a href="/new/nepal">16 November 2022</a>
                                </div>
                                <h6><a href="/new/nepal">NBI - NEPAL Delegations Explores & Learning of Microbanking in BRI ... </a></h6>
                            </div>
                        </li>
                    </ul>
                </div>

                <div class="blogsidebar__content__wraper__2" data-aos="fade-up">
                    <h4 class="sidebar__title">Photo Gallery</h4>
                    <div class="photo__gallery__img">

                        <div class="single__gallery__img">
                            <img src="https://brileaf.id/assets/galeri/marshall-2.jpg" alt="photo">
                            <div class="gallery__icon">
                                <a class="popup__img" href="https://brileaf.id/assets/galeri/marshall-2.jpg" style="width: 500px; height:400px;"> <i class="icofont-eye-alt"></i></a>
                            </div>
                        </div>
                        <div class="single__gallery__img">
                            <img src="https://brileaf.id/assets/galeri/marshall-4.jpg" alt="photo">
                            <div class="gallery__icon">
                                <a class="popup__img" href="https://brileaf.id/assets/galeri/marshall-4.jpg" style="width: 500px; height:400px;"> <i class="icofont-eye-alt"></i></a>
                            </div>
                        </div>
                        <div class="single__gallery__img">
                            <img src="https://brileaf.id/assets/galeri/marshall-6.jpg" alt="photo">
                            <div class="gallery__icon">
                                <a class="popup__img" href="https://brileaf.id/assets/galeri/marshall-6.jpg" style="width: 500px; height:400px;"> <i class="icofont-eye-alt"></i></a>
                            </div>
                        </div>
                        <div class="single__gallery__img">
                            <img src="https://brileaf.id/assets/galeri/marshall-8.jpg" alt="photo">
                            <div class="gallery__icon">
                                <a class="popup__img" href="https://brileaf.id/assets/galeri/marshall-8.jpg" style="width: 500px; height:400px;"> <i class="icofont-eye-alt"></i></a>
                            </div>
                        </div>
                        <div class="single__gallery__img">
                            <img src="https://brileaf.id/assets/galeri/marshall-9.jpg" alt="photo">
                            <div class="gallery__icon">
                                <a class="popup__img" href="https://brileaf.id/assets/galeri/marshall-9.jpg" style="width: 500px; height:400px;"> <i class="icofont-eye-alt"></i></a>
                            </div>
                        </div>
                        <div class="single__gallery__img">
                            <img src="https://brileaf.id/assets/galeri/marshall-10.jpg" alt="photo">
                            <div class="gallery__icon">
                                <a class="popup__img" href="https://brileaf.id/assets/galeri/marshall-10.jpg" style="width: 500px; height:400px;"> <i class="icofont-eye-alt"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection(); ?>