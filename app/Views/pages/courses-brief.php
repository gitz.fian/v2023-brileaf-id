<?= $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>

<!-- breadcrumbarea__section__start -->
<div class="breadcrumbarea" data-aos="fade-up">
  <div class="container">
    <div class="row">
      <div class="col-xl-12">
        <div class="breadcrumb__content__wraper">
          <div class="breadcrumb__title">
            <h2 class="heading">BRIef</h2>
          </div>
          <div class="breadcrumb__inner">

          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="shape__icon__2">
    <img class=" shape__icon__img shape__icon__img__1" src="<?= base_url('img/herobanner/herobanner__1.png'); ?>" alt="photo">
    <img class=" shape__icon__img shape__icon__img__2" src="<?= base_url('img/herobanner/herobanner__2.png'); ?>" alt="photo">
    <img class=" shape__icon__img shape__icon__img__3" src="<?= base_url('img/herobanner/herobanner__3.png'); ?>" alt="photo">
    <img class=" shape__icon__img shape__icon__img__4" src="<?= base_url('img/herobanner/herobanner__5.png'); ?>" alt="photo">
  </div>
</div>
<!-- breadcrumbarea__section__end-->
<!-- program brileaf -->
<div class="populerarea sp_top_80 sp_bottom_50">
  <div class="container">
    <div class="row populerarea__wraper" data-aos="fade-up">
      <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
        <div class="populerarea__heading heading__underline">
          <div class="default__small__button">Our Courses</div>
          <h2> <span>BRIef</span></h2>
        </div>
      </div>
      <div class="breadcrumb__content__wraper text-white" style="border-radius:25px; background-color:#106eea; ">
        BRI International Delegation Forum (BRIef) is designed to help banks and other MFIs to develop microfinance operations in their own markets. BRIef is supported by a group of experienced microfinance specialist and field staff. By participating in BRIef, you can learn from BRI's success in implementing micro banking in Indonesia as well as our successful experience in overcoming obstacles in running micro banking during Covid-19 Pandemic. Our approach and methodology applied in the program are both theory and it's implementation in the field.
      </div>
    </div>
  </div>
</div>
<!-- end program brileaf -->

<!-- program brileaf -->
<div class="expart__teacher sp_top_10 sp_bottom_10">
  <div class="container">
    <div class="row">
      <div class="col-xl-12" data-aos="fade-up">
        <div class="section__title__2 text-center">
          <div class="section__title__heading__2 section__title__heading__3 heading__fontsize__2">
            <h2>Our Portofolio</h2>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12" data-aos="fade-up">
        <div class="single__expart__teacher">
          <div class="teacher__img">
            <img src="https://brileaf.id/assets/landingpage/gsb.png" style="width:295px; height:295px;" alt="teacher">
          </div>
        </div>
      </div>
      <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12" data-aos="fade-up">
        <div class="single__expart__teacher">
          <div class="teacher__img">
            <img src="https://brileaf.id/assets/landingpage/marshall.png" style="width:295px; height:295px;" alt="teacher">
          </div>
        </div>
      </div>
      <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12" data-aos="fade-up">
        <div class="single__expart__teacher">
          <div class="teacher__img">
            <img src="https://brileaf.id/assets/landingpage/aemfi.png" style="width:295px; height:295px;" alt="teacher">
          </div>
        </div>
      </div>
      <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12" data-aos="fade-up">
        <div class="single__expart__teacher">
          <div class="teacher__img">
            <!-- <a href="#" target="_blank"> -->
            <img src="https://brileaf.id/assets/landingpage/nbi2.png" style="width:295px; height:295px;" alt="teacher">
            <!-- </a> -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end program brileaf -->

<!-- Course Preview -->
<div class="expart__teacher sp_top_50 sp_bottom_10">
  <div class="container">
    <div class="row">
      <div class="col-xl-12" data-aos="fade-up">
        <div class="section__title__2 text-center">
          <div class="section__title__heading__2 section__title__heading__3 heading__fontsize__2">
            <h2>Courses Preview</h2>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 d-flex" data-aos="fade-up">
        <div class="single__service">
          <div class="service__img">
            <div class="service__bg__img">
              <svg class="service__icon__bg" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M63.3775 44.4535C54.8582 58.717 39.1005 53.2202 23.1736 47.5697C7.2467 41.9192 -5.18037 32.7111 3.33895 18.4477C11.8583 4.18418 31.6595 -2.79441 47.5803 2.85105C63.5011 8.49652 71.8609 30.2313 63.3488 44.4865L63.3775 44.4535Z" fill="#5F2DED" fill-opacity="0.05" />
              </svg>
            </div>
          </div>
          <!-- <a href="#" target="_blank"> -->
            <div class="card h-70 shadow-sm">
              <img src="https://brileaf.id/assets/img/modul/compro.jpg" class="card-img-top" alt="...">
            </div>
          </a>
          <div class="service__content">
            <h3>
              <a href="#">Company Profile BRI Research Institute</a>
            </h3>
          </div>
          <div class="service__small__img">
            <svg class="icon__hover__img" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M16.5961 10.265L19 1.33069L10.0022 3.73285L1 6.1306L7.59393 12.6627L14.1879 19.1992L16.5961 10.265Z" stroke="#FFB31F" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
            </svg>
          </div>
        </div>
      </div>
      <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 d-flex" data-aos="fade-up">
        <div class="single__service">
          <div class="service__img">
            <div class="service__bg__img">
              <svg class="service__icon__bg" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M63.3775 44.4535C54.8582 58.717 39.1005 53.2202 23.1736 47.5697C7.2467 41.9192 -5.18037 32.7111 3.33895 18.4477C11.8583 4.18418 31.6595 -2.79441 47.5803 2.85105C63.5011 8.49652 71.8609 30.2313 63.3488 44.4865L63.3775 44.4535Z" fill="#5F2DED" fill-opacity="0.05" />
              </svg>
            </div>
          </div>
          <!-- <a href="#" target="_blank"> -->
            <div class="card h-70 shadow-sm">
              <img src="https://brileaf.id/assets/img/modul/thriving.jpg" class="card-img-top" alt="...">
            </div>
          </a>
          <div class="service__content">
            <h3>
              <a href="#">Thriving To Give Value For Indonesia Towards The Next Gen Human Capital</a>
            </h3>
          </div>
          <div class="service__small__img">
            <svg class="icon__hover__img" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M16.5961 10.265L19 1.33069L10.0022 3.73285L1 6.1306L7.59393 12.6627L14.1879 19.1992L16.5961 10.265Z" stroke="#FFB31F" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
            </svg>
          </div>
        </div>
      </div>
      <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 d-flex" data-aos="fade-up">
        <div class="single__service">
          <div class="service__img">
            <div class="service__bg__img">
              <svg class="service__icon__bg" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M63.3775 44.4535C54.8582 58.717 39.1005 53.2202 23.1736 47.5697C7.2467 41.9192 -5.18037 32.7111 3.33895 18.4477C11.8583 4.18418 31.6595 -2.79441 47.5803 2.85105C63.5011 8.49652 71.8609 30.2313 63.3488 44.4865L63.3775 44.4535Z" fill="#5F2DED" fill-opacity="0.05" />
              </svg>
            </div>
          </div>
          <!-- <a href="#" target="_blank"> -->
            <div class="card h-70 shadow-sm">
              <img src="https://brileaf.id/assets/img/modul/levareging.jpg" class="card-img-top" alt="...">
            </div>
          </a>
          <div class="service__content">
            <h3>
              <a href="#">Leveraging Indonesian Ultra-Micro for Resilience & Economic Growth</a>
            </h3>
          </div>
          <div class="service__small__img">
            <svg class="icon__hover__img" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M16.5961 10.265L19 1.33069L10.0022 3.73285L1 6.1306L7.59393 12.6627L14.1879 19.1992L16.5961 10.265Z" stroke="#FFB31F" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
            </svg>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Course Preview -->

<!-- contact__form__start -->
<div class="populerarea__2 sp_top_50 sp_bottom_50">
  <div class="container">
    <div class="row">
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 about__wrap__content aos-init aos-animate" data-aos="fade-up">
        <div class="service__animate__shape__1">
          <img src="img/service/service__shape__1.png" alt="">
        </div>
        <div class="populerarea__content__wraper__2">


          <div class="populerarea__content__2">
            <p class="populerarea__para__2">The type and project details can be customized according to needs*</p>
          </div>
          <div class="populerarea__button__2">
            <a class="default__button" href="/contact">Contact Us
              <i class="icofont-long-arrow-right"></i>
            </a>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
<!-- contact__form__end-->

<?= $this->endSection(); ?>