<?= $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>


<!-- program brileaf -->
<div class="populerarea sp_top_80 sp_bottom_50">
    <div class="container">
        <div class="row populerarea__wraper" data-aos="fade-up">
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                <div class="populerarea__heading heading__underline">
                    <div class="default__small__button">Our Courses</div>
                    <h2>BRIef <span>Courses</span></h2>
                </div>
            </div>
        </div>
<!-- end program brileaf -->

<?= $this->endSection(); ?>